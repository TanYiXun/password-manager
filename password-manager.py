from tkinter import *
from tkinter import ttk, filedialog, messagebox
from Crypto.Cipher import AES
import base64
import os
import sys

def center(win):
    win.update_idletasks()
    width = win.winfo_width()
    height = win.winfo_height()
    x = (win.winfo_screenwidth() // 2) - (width // 2)
    y = (win.winfo_screenheight() // 2) - (height // 2)
    win.geometry('{}x{}+{}+{}'.format(width, height, x, y))

def new_entry():
    window = Toplevel(root)
    center(window)
    Label(window, text="Website").grid(row=0, sticky=W)
    Label(window, text="Username").grid(row=1, sticky=W)
    Label(window, text="Password").grid(row=2, sticky=W)

    website = Entry(window)
    username = Entry(window)
    password = Entry(window)

    website.grid(row=0, column=1)
    username.grid(row=1, column=1)
    password.grid(row=2, column=1)

    def add_entry():
        w = website.get()
        u = username.get()
        p = password.get()

        if w and u and p:
            encp = base64.b64encode(cipher.encrypt(p.rjust(32))).decode()

            entry = "{}\t{}\t{}\n".format(w, u, encp)

            #check if row exists in file
            with open("password.txt", "r") as f:
                for line in f:
                    linelist = line.split("\t")
                    #if exact match, give error
                    if entry == line:
                        print("Error: Duplicate entry for website:{}".format(w))
                        window.destroy()
                        messagebox.showerror("Error", "Duplicate entry for website:{}".format(w))
                        return
                    #if website and username match, update row
                    #elif w == linelist[0]:
                    #    print("Updated entry for website:{}".format(w))
                    #
                    #    window.destroy()
                f.close()

            #enter row if previous check passes
            with open("password.txt", "a+") as f:
                print("Success entry for website:{}".format(w))
                window.destroy()
                messagebox.showinfo("Success", "Success entry for website:{}".format(w))
                f.write(entry)
                f.close()
        else:
            print("Error: values not filled up")
            window.destroy()
            messagebox.showerror("Error", "Values not filled up!")

    Button(window, text='Close', command=window.destroy).grid(row=3, column=0, sticky=W, pady=4, padx=4)
    Button(window, text='Submit', command=add_entry).grid(row=3, column=1, sticky=W, pady=4, padx=4)

def show_entries():
    window = Toplevel(root)
    window.geometry("500x270")
    center(window)

    frame = Frame(window)
    frame.pack()

    tree = ttk.Treeview(frame, columns = (1,2,3), height = 10, show = "headings")
    tree.pack(side = 'left')

    def copy(a):
        val = tree.item(tree.focus())['values']
        col = int(tree.identify_column(a.x).replace("#","")) - 1
        window.clipboard_clear()
        window.clipboard_append(val[col])
        window.update() # now it stays on the clipboard after the window is closed

    tree.bind('<Control-c>', copy)

    tree.heading(1, text="Website")
    tree.heading(2, text="Username")
    tree.heading(3, text="Password")

    tree.column(1, width=100)
    tree.column(2, width=100)
    tree.column(3, width=200)

    scroll = ttk.Scrollbar(frame, orient="vertical", command=tree.yview)
    scroll.pack(side = 'right', fill = 'y')

    tree.configure(yscrollcommand=scroll.set)

    with open("password.txt", "r") as f:
        try:
            for line in f:
                linelist = line.split("\t")
                tree.insert('', 'end', values = (linelist[0], linelist[1], cipher.decrypt(base64.b64decode(linelist[2])).strip().decode()))
                #print(line.rstrip())
        except UnicodeDecodeError as e:
            print("Secret Key Incorrect!")
            window.destroy()
            messagebox.showerror("Error", "Secret Key Incorrect")
            sys.exit()
        f.close()

def main_panel():
    root.deiconify()
    global secret_key,cipher
    secret_key = entrypass.get().zfill(32) # create new & store somewhere safe
    cipher = AES.new(secret_key,AES.MODE_ECB) # never use ECB in strong systems obviously
    #check whether password is correct
    with open("password.txt", "r") as f:
        try:
            for line in f:
                linelist = line.split("\t")
                cipher.decrypt(base64.b64decode(linelist[2])).strip().decode()
        except UnicodeDecodeError as e:
            print("Secret Key Incorrect!")
            window.destroy()
            messagebox.showerror("Error", "Secret Key Incorrect")
            sys.exit()
        f.close()
    window.destroy()
    center(root)
    Button(root, text="New Entry", command=new_entry).grid(row=0, column=0, sticky=W, pady=4, padx=4)
    Button(root, text="Show all Entries", command=show_entries).grid(row=0, column=1, sticky=W, pady=4, padx=4)

#main
secret_key = None # create new & store somewhere safe
cipher = None # never use ECB in strong systems obviously

#check whether psasword file exists, if not create one
if not os.path.exists("password.txt"):
    open("password.txt", 'w').close()

#main window
root = Tk()
root.title("Password Manager")
root.withdraw()

#login with secret Key
window = Toplevel(root)
center(window)
Label(window, text="Secret Key").grid(row=0, sticky=W)

entrypass = Entry(window)
entrypass.grid(row=0, column=1)

Button(window, text='Close', command=root.destroy).grid(row=1, column=0, sticky=W, pady=4, padx=4)
Button(window, text='Submit', command=main_panel).grid(row=1, column=1, sticky=W, pady=4, padx=4)

#main window stuffs
center(root)
root.mainloop()
