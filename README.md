# Password Manager :lock:

### Overview
This is a basic python program written for a self project, and has some basic security implementation such as encryption using AES. The program is built as a simple GUI password manager, using python's __tkinter__ module.

### Prerequisite
###### Python
The version of python used to build this program is python3, and below are the modules used within. Run ```pip3 install <module>```, if your computer does not have the neccessary modules.
* tkinter
* base64
* pycrypto
* os
* sys

### Running the program
The way to run the ```password-manager.py``` program is pretty simple, just type ```python3 password-manager.py```. If it is the first run, the program will create a __password.txt__ file to store all the records, in the format which stores the website and username in plaintext, but password encrypted.

When the program is started, the first GUI will pop up and request for the password. Only if the password is entered correctly would you be allowed to continue. If running for first time, the password entered will be used in the future.

The next GUI will show two options, one is to __Create__ a new entry, and another one is to __Show__ all entries. When viewing all entries, if you wish to copy any values in the table, just ```left-click``` on the value, followed by ```Ctrl-C```, and it will be in your clipboard. The rest is self-explanatory.